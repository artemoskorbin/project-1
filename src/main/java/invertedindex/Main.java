package invertedindex;

import invertedindex.file.DocumentWriter;

import java.util.List;

import static java.util.Arrays.asList;

public class Main {

    public static void main(String[] args) {
        List<String> argsList = asList(args);
        RankTable table = new RankTable();
        table.setSourcePath(argsList.get(0));
        table.readFolder();
        DocumentWriter writer = new DocumentWriter();
        writer.save(table.getTable());
    }
}
