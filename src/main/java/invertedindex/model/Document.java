package invertedindex.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Document {

    private String name;
    private long size;
    private long lastModified;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null && o.getClass() != this.getClass()) return false;
        Document doc = (Document) o;
        return doc.getName() != null && doc.getName().equals(this.getName()) && (doc.getSize() == this.getSize());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (name != null ? 0 : name.hashCode());
        Long sizeLong = size;
        hash = 31 * hash + (sizeLong.hashCode());
        return hash;
    }

    @Override
    public String toString() {
        return name;
    }

}
