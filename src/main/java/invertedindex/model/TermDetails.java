package invertedindex.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TermDetails {

    private TermIndexer termIndexer = new TermIndexer();
    private Map<Document, Integer> entriesToFile = new HashMap<>();


    public void addFile(File file, int count) {
        entriesToFile.put(new Document(file.getName(), file.length(), file.lastModified()), count);
    }

    public Set<Document> getIndexedFileEntries() {
        return entriesToFile.keySet();
    }

    public Double getIndex() {
        double sumIndex = 0;
        for (Map.Entry<Document, Integer> entry : entriesToFile.entrySet()
        ) {
            sumIndex += termIndexer.getIndex(entry);
        }
        return sumIndex;
    }
}
