package invertedindex.model;

import invertedindex.ApplicationProperties;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Map;

public class TermIndexer {
    public double getIndex(Map.Entry<Document, Integer> entry) {

        try {
            ApplicationProperties.readProperties();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Instant instant = LocalDate.now().atStartOfDay().toInstant(OffsetDateTime.now().getOffset());
        long millis = instant.toEpochMilli();
        return ApplicationProperties.k1 * entry.getKey().getSize() + Math.log(entry.getValue()) / Math.log(ApplicationProperties.k2) + 1 / Math.log(millis - entry.getKey().getLastModified()) / Math.log(ApplicationProperties.k3);
    }

}
