package invertedindex.file;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

@Slf4j
public class DocumentReader {

    public static String read(File file) throws IOException {
        return Files.readString(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
    }

    private String getFormattedTextOfDocument(File file) throws IOException {
        return DocumentReader.read(file)
                .replaceAll("[^a-zA-Z\\s]", "")
                .replaceAll("\\n", " ")
                .toLowerCase();
    }

    public ArrayList<String> getTerms(File file) {
        ArrayList<String> terms = null;
        try {
            terms = new ArrayList<>(Arrays.asList(getFormattedTextOfDocument(file).split(" ")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        terms.removeIf(term -> term.length() < 3);
        log.info("File {} read", file);
        return terms;
    }

}
