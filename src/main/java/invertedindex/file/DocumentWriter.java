package invertedindex.file;

import invertedindex.model.Document;
import invertedindex.model.TermDetails;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class DocumentWriter {
    private static String path = "src/main/resources/res";
    private static final String fileName = "/res.txt";

    private void createPath() {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdir();
        }

        path += fileName;
    }

    public void save(Map<String, TermDetails> table) {
        createPath();
        Comparator<TermDetails> byIndex = Comparator.comparing(TermDetails::getIndex).reversed();
        LinkedHashMap<String, TermDetails> map = table.entrySet().stream().sorted(Map.Entry.comparingByValue(byIndex))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        File file = Paths.get(path).toFile();

        try {
            BufferedWriter bf = new BufferedWriter(new FileWriter(file));
            for (Map.Entry<String, TermDetails> entry : map.entrySet()) {
                bf.write(entry.getKey() + "   ");
                for (Document document : entry.getValue().getIndexedFileEntries()) {
                    bf.write(document.getName().replaceFirst("[.][^.]+$", " ") + " ");
                }

                bf.newLine();
            }
            log.info("inverted index created");
            bf.flush();
            bf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
