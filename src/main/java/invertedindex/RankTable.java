package invertedindex;

import invertedindex.exception.FileDuplicateException;
import invertedindex.file.DocumentReader;
import invertedindex.model.Document;
import invertedindex.model.TermDetails;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Data
@Slf4j
public class RankTable {

    private DocumentReader documentReader = new DocumentReader();
    private String sourcePath;
    private Map<String, TermDetails> table = new HashMap<>();
    List<Document> documents = new ArrayList<>();


    public void putTable(String filePath) {
        ArrayList<String> terms = documentReader.getTerms(new File(filePath));
        Map<String, Long> termCount = terms.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        termCount.forEach((term, count) -> {
            TermDetails termDetail = table.getOrDefault(term, new TermDetails());
            termDetail.addFile(new File(filePath), count.intValue());
            table.put(term, termDetail);
        });

        log.info("PutTable  {} read", filePath);
    }


    public void readFolder() {
        try (Stream<Path> paths = Files.walk(Paths.get(sourcePath))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(path -> {
                        readFile(path);
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Folder {} read", sourcePath);
    }

    private void readFile(Path path) {
        File file = path.toFile();
        long fileSize = file.length();
        long lastModified = file.lastModified();
        String name = file.getName().replaceFirst("[.][^.]+$", " ");
        Document doc = new Document(name, fileSize, lastModified);

        try {
            if (documents.contains(doc)) {
                throw new FileDuplicateException("File already exists");
            }
            putTable(file.getAbsolutePath());

        } catch (FileDuplicateException e) {
            log.warn("Duplicated file was found");
        }

    }
}
