package invertedindex.exception;



public class FileDuplicateException extends Exception {
    public FileDuplicateException(String errorMessage){
        super(errorMessage);
    }

}
